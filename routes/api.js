const express = require("express");
let users = require("./../db/user.json");
const app = express();

app.use(express.Router());
app.use(express.json());

app.get("/v1/user", (req, res, next) => res.json(users));
app.get("/v1/user/:id", (req, res, next) => {
    const user = users.find((item) => item.id === +req.params.id);
    if (user) {
        res.status(200).json(user);
    } else {
        res.status(200).send("ID not found");
    }
});

app.post("/v1/user", (req, res, next) => {
    const { username, email, password } = req.body;
    const id = users[users.length - 1].id + 1;
    const user = {
        id,
        username,
        email,
        password,
    };

    users.push(user);
    res.status(201).json(user);
});

app.put("/v1/user/:id", (req, res, next) => {
    let user = users.find((item) => item.id === +req.params.id);
    const params = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    };
    user = { ...user, ...params };
    users = users.map((item) => (item.id === user.id ? user : item));
    res.status(200).json(user);
});

app.delete("/v1/user/:id", (req, res, next) => {
    users = users.filter((item) => item.id !== +req.params.id);
    res.status(200).json({
        message: `Users id ${req.params.id} has been deleted!`,
    });
});

module.exports = app;
