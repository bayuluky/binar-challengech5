const express = require("express");
const router = express();

router.use(express.Router());
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.get("/", (req, res, next) => {
    res.render("landingpage");
});

// router.post("/", (req, res, next) => {
//     res.render("landingpage");
// });

module.exports = router;
